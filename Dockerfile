FROM debian:bullseye

ENV MYDEBIAN11DOCKER_VERSION build-target
ENV MYDEBIAN11DOCKER_VERSION latest
ENV MYDEBIAN11DOCKER_VERSION stable
ENV MYDEBIAN11DOCKER_IMAGE registry.gitlab.com/george-pon/mydebian11docker

ENV DEBIAN_FRONTEND noninteractive

# set locale
RUN apt-get update && \
    apt-get install -y locales  apt-transport-https  ca-certificates  software-properties-common && \
    localedef -i ja_JP -c -f UTF-8 -A /usr/share/locale/locale.alias ja_JP.UTF-8 && \
    apt-get clean
ENV LANG ja_JP.utf8

# set timezone
# humm. failed at GitLab CI.
# RUN rm -f /etc/localtime ; ln -fs /usr/share/zoneinfo/Asia/Tokyo /etc/localtime
RUN rm /etc/localtime ; echo Asia/Tokyo > /etc/timezone ; dpkg-reconfigure -f noninteractive tzdata
ENV TZ Asia/Tokyo

# install man pages
RUN apt-get install -y man-db  manpages && apt-get clean


#         mongodb-clients \

# install etc utils
RUN apt-get install -y \
        ansible \
        bash-completion \
        connect-proxy \
        curl \
        dnsutils \
        emacs-nox \
        expect \
        gettext \
        git \
        gnupg2 \
        iproute2 \
        jq \
        lsof \
        make \
        netcat \
        net-tools \
        postgresql-client \
        procps \
        python3-pip \
        rsync \
        sudo \
        tcpdump \
        traceroute \
        tree \
        unzip \
        vim \
        w3m \
        wget \
        zip \
    && apt-get clean all

# install azure cli 
RUN MACHINE=$( uname -m ) && \
    if [ x"$MACHINE"x = x"x86_64"x ] ; then \
    apt-get update && \
    apt-get install ca-certificates curl apt-transport-https lsb-release gnupg && \
    curl -sL https://packages.microsoft.com/keys/microsoft.asc |  gpg --dearmor | tee /etc/apt/trusted.gpg.d/microsoft.gpg > /dev/null && \
    AZ_REPO=$(lsb_release -cs) && \
    echo "deb [arch=amd64] https://packages.microsoft.com/repos/azure-cli/ $AZ_REPO main" | tee /etc/apt/sources.list.d/azure-cli.list && \
    apt-get update && \
    apt-get install azure-cli && apt-get clean all ; \
    fi

# install docker client
RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
    MACHINE=$( uname -m ) && \
    if [ x"$MACHINE"x = x"armv7l"x ]; then  ARCH=armhf;   elif [ x"$MACHINE"x = x"x86_64"x ]; then  ARCH=amd64;  elif [ x"$MACHINE"x = x"aarch64"x ]; then  ARCH=arm64;  fi && \
    add-apt-repository  "deb [arch=${ARCH}] https://download.docker.com/linux/debian $(lsb_release -cs) stable" && \
    apt-get update && \
    apt-get install -y docker-ce-cli && apt-get clean

# install docker-compose
RUN apt-get install -y docker-compose && apt-get clean

# install kubectl CLI
# RUN curl -fsSL https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add - && \
#     echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" >/etc/apt/sources.list.d/kubernetes.list && \
#     apt-get update && \
#     apt-get install -y kubectl && apt-get clean

# install kubectl CLI
RUN if [ x"$(uname -m)"x = x"x86_64"x ] ; then \
    KUBE_VERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt) && \
    KUBE_ARCH=amd64 && \
    curl -LO "https://storage.googleapis.com/kubernetes-release/release/${KUBE_VERSION}/bin/linux/${KUBE_ARCH}/kubectl" && \
    chmod +x kubectl && \
    mv ./kubectl /usr/bin ; \
    fi
RUN if [ x"$(uname -m)"x = x"aarch64"x ] ; then \
    KUBE_VERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt) && \
    KUBE_ARCH=arm64 && \
    curl -LO "https://storage.googleapis.com/kubernetes-release/release/${KUBE_VERSION}/bin/linux/${KUBE_ARCH}/kubectl" && \
    chmod +x kubectl && \
    mv ./kubectl /usr/bin ; \
    fi
RUN if [ x"$(uname -m)"x = x"armv7l"x ] ; then \
    KUBE_VERSION=$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt) && \
    KUBE_ARCH=arm && \
    curl -LO "https://storage.googleapis.com/kubernetes-release/release/${KUBE_VERSION}/bin/linux/${KUBE_ARCH}/kubectl" && \
    chmod +x kubectl && \
    mv ./kubectl /usr/bin ; \
    fi

# install helm CLI v2.9.1
#ENV HELM_CLIENT_VERSION v2.9.1
#RUN curl -fLO https://storage.googleapis.com/kubernetes-helm/helm-${HELM_CLIENT_VERSION}-linux-amd64.tar.gz && \
#    tar xzf  helm-${HELM_CLIENT_VERSION}-linux-amd64.tar.gz && \
#    /bin/cp  linux-amd64/helm   /usr/bin && \
#    /bin/rm -rf rm helm-${HELM_CLIENT_VERSION}-linux-amd64.tar.gz linux-amd64

# install helm CLI from https://github.com/helm/helm/releases
ENV HELM3_VERSION v3.9.4
RUN if [ x"$(uname -m)"x = x"x86_64"x ] ; then \
    curl -fLO https://get.helm.sh/helm-${HELM3_VERSION}-linux-amd64.tar.gz && \
    tar xzf  helm-${HELM3_VERSION}-linux-amd64.tar.gz && \
    /bin/cp  linux-amd64/helm   /usr/bin && \
    /bin/cp  linux-amd64/helm   /usr/bin/helm3 && \
    /bin/rm -rf rm helm-${HELM3_VERSION}-linux-amd64.tar.gz linux-amd64 ; \
    fi
RUN if [ x"$(uname -m)"x = x"aarch64"x ] ; then \
    curl -fLO https://get.helm.sh/helm-${HELM3_VERSION}-linux-arm64.tar.gz && \
    tar xzf  helm-${HELM3_VERSION}-linux-arm64.tar.gz && \
    /bin/cp  linux-arm64/helm   /usr/bin && \
    /bin/cp  linux-arm64/helm   /usr/bin/helm3 && \
    /bin/rm -rf rm helm-${HELM3_VERSION}-linux-arm64.tar.gz linux-arm64 ; \
    fi

# install kompose v1.18.0
ENV KOMPOSE_VERSION v1.18.0
RUN curl -fLO https://github.com/kubernetes/kompose/releases/download/${KOMPOSE_VERSION}/kompose-linux-amd64.tar.gz && \
    tar xzf kompose-linux-amd64.tar.gz && \
    chmod +x kompose-linux-amd64 && \
    mv kompose-linux-amd64 /usr/bin/kompose && \
    rm kompose-linux-amd64.tar.gz

# install stern from https://github.com/wercker/stern/releases/
ENV STERN_VERSION 1.11.0
RUN curl -fLO https://github.com/wercker/stern/releases/download/${STERN_VERSION}/stern_linux_amd64 && \
    chmod +x stern_linux_amd64 && \
    mv stern_linux_amd64 /usr/bin/stern

# install kustomize
ENV KUSTOMIZE_VERSION 1.0.11
RUN curl -fLO https://github.com/kubernetes-sigs/kustomize/releases/download/v${KUSTOMIZE_VERSION}/kustomize_${KUSTOMIZE_VERSION}_linux_amd64 && \
    chmod +x kustomize_${KUSTOMIZE_VERSION}_linux_amd64 && \
    mv kustomize_${KUSTOMIZE_VERSION}_linux_amd64 /usr/bin/kustomize

# install kubectx, kubens. see https://github.com/ahmetb/kubectx
RUN curl -fLO https://raw.githubusercontent.com/ahmetb/kubectx/master/kubectx && \
    curl -fLO https://raw.githubusercontent.com/ahmetb/kubectx/master/kubens && \
    chmod +x kubectx kubens && \
    mv kubectx kubens /usr/local/bin

# install kubeval ( validate Kubernetes yaml file to Kube-API )
ENV KUBEVAL_VERSION 0.7.3
RUN curl -fLO https://github.com/garethr/kubeval/releases/download/$KUBEVAL_VERSION/kubeval-linux-amd64.tar.gz && \
    tar xf kubeval-linux-amd64.tar.gz && \
    cp kubeval /usr/local/bin && \
    /bin/rm kubeval-linux-amd64.tar.gz

# install kubetest ( lint kubernetes yaml file )
ENV KUBETEST_VERSION 0.1.1
RUN curl -fLO https://github.com/garethr/kubetest/releases/download/$KUBETEST_VERSION/kubetest-linux-amd64.tar.gz && \
    tar xf kubetest-linux-amd64.tar.gz && \
    cp kubetest /usr/local/bin && \
    /bin/rm kubetest-linux-amd64.tar.gz

# install yamlsort from https://github.com/george-pon/yamlsort
ENV YAMLSORT_VERSION v0.1.19
RUN if [ x"$(uname -m)"x = x"x86_64"x ] ; then \
    curl -fLO https://github.com/george-pon/yamlsort/releases/download/${YAMLSORT_VERSION}/linux_amd64_yamlsort_${YAMLSORT_VERSION}.tar.gz && \
    tar xzf linux_amd64_yamlsort_${YAMLSORT_VERSION}.tar.gz && \
    chmod +x linux_amd64_yamlsort && \
    mv linux_amd64_yamlsort /usr/bin/yamlsort && \
    rm linux_amd64_yamlsort_${YAMLSORT_VERSION}.tar.gz ; \
    fi
RUN if [ x"$(uname -m)"x = x"aarch64"x ] ; then \
    curl -fLO https://github.com/george-pon/yamlsort/releases/download/${YAMLSORT_VERSION}/linux_arm64_yamlsort_${YAMLSORT_VERSION}.tar.gz && \
    tar xzf linux_arm64_yamlsort_${YAMLSORT_VERSION}.tar.gz && \
    chmod +x linux_arm64_yamlsort && \
    mv linux_arm64_yamlsort /usr/bin/yamlsort && \
    rm linux_arm64_yamlsort_${YAMLSORT_VERSION}.tar.gz ; \
    fi



ADD docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh

ADD bashrc       /root/.bashrc
ADD inputrc      /root/.inputrc
ADD bash_profile /root/.bash_profile
ADD vimrc        /root/.vimrc
ADD emacsrc      /root/.emacs
ADD bin /usr/local/bin
RUN chmod +x /usr/local/bin/*.sh

ENV HOME /root
ENV ENV $HOME/.bashrc

# add sudo user
# https://qiita.com/iganari/items/1d590e358a029a1776d6 Dockerコンテナ内にsudoユーザを追加する - Qiita
# ユーザー名 debian
# パスワード hogehoge
RUN groupadd -g 1000 debian && \
    useradd  -g      debian -G sudo -m -s /bin/bash debian && \
    echo 'debian:hogehoge' | chpasswd && \
    echo 'Defaults visiblepw'            >> /etc/sudoers && \
    echo 'debian ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# use normal user debian
# USER debian

CMD ["/usr/local/bin/docker-entrypoint.sh"]

