# mydebian11docker

### image and tags

* Dockerfile: https://gitlab.com/george-pon/mydebian11docker/-/blob/master/Dockerfile
* image: registry.gitlab.com/george-pon/mydebian11docker ,  tags:  build202101, stable, latest

### arch

* amd64 (x86_64 64bit)
* armhf (Raspberry Pi 4 32bit)
* arm64 (Raspberry Pi 4 64bit)

## how to use

### run via Docker

```
function docker-run-mydebian11docker() {
    docker pull registry.gitlab.com/george-pon/mydebian11docker:latest
    ${WINPTY_CMD} docker run -i -t --rm \
        -e http_proxy=${http_proxy} -e https_proxy=${https_proxy} -e no_proxy="${no_proxy}" \
        registry.gitlab.com/george-pon/mydebian11docker:latest
}
docker-run-mydebian11docker
```

### run via Kubernetes

```
function kube-run-mydebian11docker() {
    local tmp_no_proxy=$( echo $no_proxy | sed -e 's/,/\,/g' )
    ${WINPTY_CMD} kubectl run mydebian11docker -i --tty --image=registry.gitlab.com/george-pon/mydebian11docker:latest --rm \
        --env="http_proxy=${http_proxy}" --env="https_proxy=${https_proxy}" --env="no_proxy=${tmp_no_proxy}"
}
kube-run-mydebian11docker
```

### run via kube-run-v.sh

* https://gitlab.com/george-pon/mydebian11docker/-/raw/master/bin/kube-run-v.sh

```
mkdir -p /home/george/podwork
cd /home/george/podwork
curl -LO https://gitlab.com/george-pon/mydebian11docker/-/raw/master/bin/kube-run-v.sh
bash ./kube-run-v.sh --image-debian
```

### what is kube-run-v.sh ?

To carry on current directory files on a "working" pod, 
kube-run-v.sh creates a "working" pod, archive current directory, copy it into the pod (by kubectl cp), extract it in the pod , and exec -i -t the pod bash --login.

When you exit from the pod, To carry out current directory files from pod,
kube-run-v.sh creates archive file in the pod, copy it form the pod (by kubectl cp), extract it in current directory(over-write).

kube-run-v.sh can also use official docker image or your own docker image with option "--image-debian" .

```
curl -LO https://gitlab.com/george-pon/mydebian11docker/-/raw/master/bin/kube-run-v.sh
bash ./kube-run-v.sh --image-debian
```

kube-run-v.sh requires bash --login, bash, tar command in the docker image.

SECURITY WARNING: kube-run-v.sh creates the serviceaccount and gives him cluster-admin role, 
then run a pod with the serviceacount.


### how to build

```
bash build-image.sh
```

